import React, { Component } from "react";
//import { render } from "react-dom";
import data from "../data/arr.json";
import  Card  from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
//import Rater from './Rater';
import Slider from '@material-ui/core/Slider';
import Tooltip from '@material-ui/core/Tooltip';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

/*
const useStyles = makeStyles({
    card: {
      width: '10px',
      borderRadius: '10px',
      borderWidth: '10px',
      
    },
  });

*/
const useStyles = makeStyles(theme => ({
    root: {
      width: 300 + theme.spacing(3) * 2,
    },
    margin: {
      height: theme.spacing(3),
    },
  }));
  
  function ValueLabelComponent(props) {
    const { children, open, value } = props;
  
    return (
      <Tooltip open={open} enterTouchDelay={0} placement="top" title={value}>
        {children}
      </Tooltip>
    );
  }
  
  ValueLabelComponent.propTypes = {
    children: PropTypes.element.isRequired,
    open: PropTypes.bool.isRequired,
    value: PropTypes.number.isRequired,
  };
  


const PrettoSlider = withStyles({
    root: {
      color: 'secondary',
      height: 8,
      width: 200,
      pointerEvents: 'none',
    },
    thumb: {
      height: 0,
      width: 0,
      backgroundColor: '#fff',
      border: '2px solid currentColor',
      marginTop: -8,
      marginLeft: -2,
      '&:focus,&:hover,&$active': {
        boxShadow: 'inherit',
      },
    },
    active: {},
    valueLabel: {
      left: 'calc(50% + 4px)',
    },
    track: {
      height: 8,
      borderRadius: 4,
    },
    rail: {
      height: 8,
      borderRadius: 4,
   
    },
  })(Slider);

//for dealing with movie list
const  newdata = data.map( ( data) =>{
    var fx = parseFloat(data.imdbRating)*10 ;     
    const items = data.genres.map((d) => <h>{d} </h>);

    
    return  (
      
      <Card key= {data.id}  style={{ maxWidth:"400px", margin:"20px"}} >
        
        <CardMedia
        style={{margin:"40px", borderRadius:"20px", maxWidth:"200px"}}
        component="img"
        src={data.posterurl}
        title={data.id}
      >
      </CardMedia>

    <h4>{data.originalTitle}  <h className="rate">{data.imdbRating}</h></h4> 

    <h6>{data.storyline}</h6>

    <Typography variant="subtitle1" color="textSecondary" >
     <div display="inline-block" >
    <h>{data.year} { items}</h> ·
          <h> {data.duration} </h>
      
      
      
    </div>  

    <ul>
    Director : <h> {data.directors}</h>
    
    Writers : <h> {data.writers}</h>
Stars: 
    </ul>
      <PrettoSlider valueLabelDisplay="off" aria-label="pretto slider" defaultValue={fx} />
      <div/>
   
   </Typography> 
 

   <Button 
   variant="contained" 
   color="secondary" 
   size="small" >
    <h4>MOVIE DETAILS</h4>
   </Button>

   <div><br/></div>

      
      </Card> 
       
     
    )
  }
)





export default class More extends Component {
   

   render() {  
     return (
            <div>  {newdata}  </div>
     )
  }
}