import React, { useState, Component } from "react";
//import { render } from "react-dom";
import data from "../data/data.json";
import  Card  from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
//import Rater from './Rater';

import Slider from '@material-ui/core/Slider';
import Tooltip from '@material-ui/core/Tooltip';
import More from './More';
import mata from '../data/arr.json';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import LoginControl from './Details';



const useStyles = makeStyles(theme => ({
    root: {
      width: 300 + theme.spacing(3) * 2,
    },
    margin: {
      height: theme.spacing(3),
    },
  }));


  function ValueLabelComponent(props) {
    const { children, open, value } = props;
  
    return (
      <Tooltip open={open} enterTouchDelay={0} placement="top" title={value}>
        {children}
      </Tooltip>
    );
  }
  
  ValueLabelComponent.propTypes = {
    children: PropTypes.element.isRequired,
    open: PropTypes.bool.isRequired,
    value: PropTypes.number.isRequired,
  };
  


const PrettoSlider = withStyles({
    root: {
      color: 'secondary',
      height: 8,
      width: 300,
      pointerEvents: 'none',
    },
    thumb: {
      height: 0,
      width: 0,
      backgroundColor: '#fff',
      border: '2px solid currentColor',
      marginTop: -8,
      marginLeft: -2,
      '&:focus,&:hover,&$active': {
        boxShadow: 'inherit',
      },
    },
    active: {},
    valueLabel: {
      left: 'calc(50% + 4px)',
    },
    track: {
      height: 8,
      borderRadius: 4,
    },
    rail: {
      height: 8,
      borderRadius: 4,
   
    },
  })(Slider);





function B(props) {

  if(props.value) {
    return <Newdata/>
  }


return <div> <Card data = {data}/> </div>
}



// A shoul map key to key on More to Items
function A(props) {

  if(props.value) {
  return <div>ON?</div>;
  }


return <div><More/></div>
}

class Newdata extends Component{


    // togglotion
    constructor(props) {
      super(props);
      this.state = {value:true};
      this.handleClick = this.handleClick.bind(this);
    }
    
    handleClick() {
      this.setState({
        value: !this.state.value
      });
      console.log('The link was clicked.' + this.state.value+ 'sf');

    
  
    }

    handClick = () => {
      this.setState({
      value :!this.state.value
      });
    }

    
  render(){
  
      return (
          <div >
            

             {
               data.map( ( data) =>{
                var fx = parseFloat(data.imdbRating)*10 ;     
                const items = data.genres.map((d) => <h>{d} </h>);
               // const [count, setCount] = useState(0);
          
               
          
          
            return  (
            
              
              <Card key= {data.id} style={{ margin:"30px"}} >
                
              <CardMedia
              style={{margin:"40px", borderRadius:"20px", maxWidth:"300px"}}
              component="img"
              src={data.posterurl}
              title={data.id}
              >
              </CardMedia>
              <h4>{data.originalTitle}</h4>  
          
              <Typography variant="subtitle1" color="textSecondary" >
              <div display="inline-block" >
              <h>{data.year} { items}</h> ·
              <h> {data.duration} </h>
           
          </div>  
          
          <div display="flex-inline" >
              <h className="rate">{data.imdbRating}</h>
              <h>/10</h> 
          </div>
            <PrettoSlider valueLabelDisplay="off" aria-label="pretto slider" defaultValue={fx} />
            <div/>
          </Typography> 
            
            
            <A key={data.id} value={this.state.value}/>
            <Button  variant="contained" color="secondary" size="small"  onClick={this.handleClick}>Toggle {this.state.value ? 'ON' : 'OFF'}</Button>

          </Card>
             
              )
            })
              
             }
          </div>
      );
  }
}
 
 


  


class MovieList extends Component {

  

  // togglotion
  constructor(props) {
    super(props);
  
    this.state = {value:true}
  }
  handleClick = () => {
    this.setState({
    value :!this.state.value
    });
  }


render(){ 

return (
 <div >


<B value={this.state.value}/>
<Button  variant="contained" color="secondary" size="small" onClick={this.handleClick}>Toggle</Button>

</div>
)
}
}



export default MovieList;