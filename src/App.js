import React from 'react';
//import logo from './armut.svg';
import './App.css';
//import 
import SearchAppBar from './Components/AppBar';
import MovieList from './Components/MovieList';
import NavigationIcon from '@material-ui/icons/Navigation';
import Fab from '@material-ui/core/Fab';
import Rater from './Components/Rater';
import LoginControl from './Components/Details';


function App() {
  return (
    <div className="App">
      <SearchAppBar/>
      <header className="App-header">
        <h2>Armut Movies</h2>

        {/* <LoginControl/> */}
         
        <MovieList/>
        
        <br/>
        
      </header>
      <Fab variant="extended">
      <NavigationIcon/>
Navigate
</Fab>
    </div>
  );
}

export default App;
