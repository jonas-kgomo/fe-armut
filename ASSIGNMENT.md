## Given

There are 4 data files under data folder.
There is a design file under design folder.

## Mission

Mission is creating a 2 pages SPA.
First one is movie list, second one is selected movie details.
Movie cards should be aligned one by one in the first page when we scroll we should see other movie's card.
You see the third one. It is not a different page. When user click the 'add to watchlist' button color should change.

## Expected

Designs are only for mobile, no expectations for desktop.
Any js framework or library welcomes (Angular, React or VueJS).
Should use a css preprocessors (less, sass).
Should use a css naming convention.

## Nice to have

We welcome any unit tests with enthusiasm.
State managment (Redux, NGRX).
Some animation for button clicks or page redirect.
Maybe SSR support :).

Please note that if you can provide a zeplin account we can share the design link.


d=v/t



> Convert Function map -- Class 
> Bind function inside function 

y= x+10


y= (x+y)0.6

10y/6=x+y

y-y/0.6= 10-y

2y-5/3y=10


P= int

A=p(1+int)

600= p(1+4i)
> 3*600= p(3+12i)
575= p(1+3i)
> 4*575 = p(4+12i)


1800-


C>A>B 

D>A
D>C

E>B










